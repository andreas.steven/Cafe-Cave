package com.cafecave.api.repository;

import org.springframework.data.repository.CrudRepository;
import com.cafecave.api.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
}