package com.cafecave.api.repository;
import com.cafecave.api.model.History;

import org.springframework.data.repository.CrudRepository;

public interface HistoryRepository extends CrudRepository<History, Integer>{
}
