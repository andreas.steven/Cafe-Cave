package com.cafecave.api.repository;
import com.cafecave.api.model.Discount;

import org.springframework.data.repository.CrudRepository;

public interface DiscountRepository extends CrudRepository<Discount, Integer> {
}