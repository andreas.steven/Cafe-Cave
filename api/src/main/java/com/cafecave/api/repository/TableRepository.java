package com.cafecave.api.repository;

import org.springframework.data.repository.CrudRepository;
import com.cafecave.api.model.Tables;

public interface TableRepository extends CrudRepository<Tables, Integer> {
}