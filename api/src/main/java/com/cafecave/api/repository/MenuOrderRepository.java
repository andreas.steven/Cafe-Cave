package com.cafecave.api.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import com.cafecave.api.model.MenuOrder;
import com.cafecave.api.model.Order;

public interface MenuOrderRepository extends CrudRepository<MenuOrder, Integer>{
    List<MenuOrder> findByOrder(Order order);
}
