package com.cafecave.api.repository;

import org.springframework.data.repository.CrudRepository;
import com.cafecave.api.model.Review;

public interface ReviewRepository extends CrudRepository<Review, Integer> {
}