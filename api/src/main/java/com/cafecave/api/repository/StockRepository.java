package com.cafecave.api.repository;

import org.springframework.data.repository.CrudRepository;

import com.cafecave.api.model.Stock;

public interface StockRepository extends CrudRepository<Stock, Integer> {
}