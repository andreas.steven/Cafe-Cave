package com.cafecave.api.repository;
import com.cafecave.api.model.Order;

import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Integer>{
}
