package com.cafecave.api.repository;

import org.springframework.data.repository.CrudRepository;

import java.sql.Date;
import java.util.List;
import com.cafecave.api.model.Reservation;
import com.cafecave.api.model.Tables;

public interface ReservationRepository extends CrudRepository<Reservation, Integer> {
    List<Reservation> findByTable(Tables table);
    List<Reservation> findByDate(Date date);
    List<Reservation> findByEmail(String email);
}