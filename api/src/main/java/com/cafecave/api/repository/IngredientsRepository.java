package com.cafecave.api.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

import com.cafecave.api.model.Menu;
import com.cafecave.api.model.Ingredients;
import com.cafecave.api.model.Stock;

public interface IngredientsRepository extends CrudRepository<Ingredients, Integer> {

    List<Ingredients> findByMenu(Menu menu);
    List<Ingredients> findByStock(Stock stock);
}