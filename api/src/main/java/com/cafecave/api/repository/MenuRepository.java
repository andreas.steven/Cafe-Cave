package com.cafecave.api.repository;

import org.springframework.data.repository.CrudRepository;
import com.cafecave.api.model.Menu;

public interface MenuRepository extends CrudRepository<Menu, Integer> {

}