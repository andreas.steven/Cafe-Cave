package com.cafecave.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cafecave.api.model.Review;
import com.cafecave.api.repository.ReviewRepository;
import com.cafecave.api.model.Menu;
import com.cafecave.api.repository.MenuRepository;

@Controller
@RequestMapping(path = "/review")
public class ReviewController {
    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private MenuRepository menuRepository;

    @PostMapping(path = "/add")
    public @ResponseBody ResponseEntity<Review> addNewReview(
        @RequestParam String description, 
        @RequestParam int menu_id, 
        @RequestParam int rating) {

        try {
            Review reviewData = new Review();
            reviewData.setDescription(description);
            reviewData.setRating(rating);
            Menu menuData = menuRepository.findById(menu_id).get();
            reviewData.setMenu(menuData);
            reviewRepository.save(reviewData);
            return ResponseEntity.ok(reviewData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/all")
    public @ResponseBody ResponseEntity<Iterable<Review>> getAllReviews() {
        try {
            return ResponseEntity.ok(reviewRepository.findAll());
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Review> getReview(@PathVariable int id) {
        try {
            Review reviewData = reviewRepository.findById(id).get();
            return ResponseEntity.ok(reviewData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}