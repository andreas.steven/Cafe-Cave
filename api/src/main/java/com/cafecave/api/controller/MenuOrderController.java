package com.cafecave.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.ArrayList;

import com.cafecave.api.dto.IngredientsAvailabilityData;
import com.cafecave.api.model.Discount;
import com.cafecave.api.model.Order;
import com.cafecave.api.model.MenuOrder;
import com.cafecave.api.model.Menu;
import com.cafecave.api.model.Ingredients;
import com.cafecave.api.repository.DiscountRepository;
import com.cafecave.api.repository.OrderRepository;
import com.cafecave.api.repository.MenuOrderRepository;
import com.cafecave.api.repository.MenuRepository;
import com.cafecave.api.repository.IngredientsRepository;

@Controller
@RequestMapping(path = "/menu_order")
public class MenuOrderController {
    @Autowired
    private DiscountRepository discountRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private MenuOrderRepository menu_orderRepository;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private IngredientsRepository ingredientRepository;

    @Autowired
    private StockController stockController;

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<MenuOrder> getAllMenuOrder() {
        return menu_orderRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<MenuOrder> getMenuOrder(@PathVariable Integer id) {
        try {
            MenuOrder menu_orderData = menu_orderRepository.findById(id).get();
            return ResponseEntity.ok(menu_orderData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }    

    @GetMapping(path = "/ingredient_by_menu/{menu_id}")
    public @ResponseBody ResponseEntity< ArrayList<IngredientsAvailabilityData> > getIngredientsAvailabilityData(
        @PathVariable int menu_id, 
        @RequestParam int menuOrder_id){

        ArrayList<IngredientsAvailabilityData> ingrDat = new ArrayList<>();
        
        try {
            List<Ingredients> ingredientData = ingredientRepository.findByMenu( menuRepository.findById(menu_id).get() );
            MenuOrder menu_order = menu_orderRepository.findById(menuOrder_id).get();

            for(int i = 0; i < ingredientData.size(); i++){
                int availableStock = ingredientData.get(i).getStock().getStock_qty();
                int requiredStock = ingredientData.get(i).getRequired_stock_qty() * menu_order.getQty();
                String stockName = ingredientData.get(i).getStock().getName();

                IngredientsAvailabilityData temp = new IngredientsAvailabilityData();
                temp.setAvailable(availableStock);
                temp.setRequired(requiredStock);
                temp.setStockName(stockName);
                if(temp.getAvailable() > temp.getRequired()){
                    temp.setIs_available(true);
                } else {
                    temp.setIs_available(false);
                }

                ingrDat.add(temp);
            }
            
            return ResponseEntity.ok(ingrDat);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(path = "/add")
    public @ResponseBody ResponseEntity<MenuOrder> addMenuOrder(
        @RequestParam int id, 
        @RequestParam int order_id,
        @RequestParam int menu_id, 
        @RequestParam int qty) {

        try {
            MenuOrder menu_orderData = new MenuOrder();
            Order orderData = orderRepository.findById(order_id).get();
            Menu menuData = menuRepository.findById(menu_id).get();
            menu_orderData.setId(id);
            menu_orderData.setQty(qty);
            menu_orderData.setSubtotal(qty * menuData.getPrice()); // Set subtotal
            orderData.setTotal(orderData.getTotal() + menu_orderData.getSubtotal()); // Set total
            menu_orderData.setOrder(orderData);
            menu_orderData.setMenu(menuData);

            menu_orderRepository.save(menu_orderData);
            orderRepository.save(orderData);
            return ResponseEntity.ok(menu_orderData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<MenuOrder> updateMenuOrder(
        @RequestParam int order_id, 
        @RequestParam int menu_id, 
        @RequestParam int qty,
        @PathVariable int id) {

        try {
            MenuOrder menu_orderData = new MenuOrder();
            menu_orderData.setId(id);
            Order orderData = orderRepository.findById(order_id).get();
            menu_orderData.setOrder(orderData);
            menu_orderData.setQty(qty);
            Menu menuData = menuRepository.findById(menu_id).get();
            menu_orderData.setMenu(menuData);
            menu_orderData.setSubtotal(qty * menuData.getPrice()); // Set subtotal
            menu_orderRepository.save(menu_orderData);
            return ResponseEntity.ok(menu_orderData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    private float count_discount(float total, float rate){
        return total = total * rate;
    }

    private void updateStockAfterPayment(Order order){
        List<MenuOrder> menu_orderData = menu_orderRepository.findByOrder(order);

        for(MenuOrder menu_order : menu_orderData){
            List<Ingredients> ingredientData = ingredientRepository.findByMenu( menu_order.getMenu() );

            for(Ingredients ingredient : ingredientData){
                stockController.updateStock(
                    ingredient.getStock().getName(), 
                    ingredient.getStock().getStock_qty() - menu_order.getQty() * ingredient.getRequired_stock_qty(), 
                    ingredient.getStock().getExp_date(), 
                    ingredient.getStock().getId()
                );
            }
        }
    }

    @PutMapping(path = "/confirm-payment/{order_id}")
    public @ResponseBody ResponseEntity<Order> setPayment(@PathVariable int order_id){
        try {
            Order orderData = orderRepository.findById(order_id).get();
            Discount discData = discountRepository.findById(orderData.getDiscount().getId()).get();
            orderData.setTotal( count_discount(orderData.getTotal(), discData.getRate()) );
            orderData.setId(order_id);
            orderRepository.save(orderData);
            updateStockAfterPayment(orderData);
            return ResponseEntity.ok(orderData);
        } catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Void> deleteMenuOrder(@PathVariable int id) {
        try {
            menu_orderRepository.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
