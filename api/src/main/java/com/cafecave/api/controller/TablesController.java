 package com.cafecave.api.controller;

 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.http.ResponseEntity;
 import org.springframework.stereotype.Controller;
 import org.springframework.web.bind.annotation.DeleteMapping;
 import org.springframework.web.bind.annotation.GetMapping;
 import org.springframework.web.bind.annotation.PathVariable;
 import org.springframework.web.bind.annotation.PostMapping;
 import org.springframework.web.bind.annotation.PutMapping;
 import org.springframework.web.bind.annotation.RequestMapping;
 import org.springframework.web.bind.annotation.RequestParam;
 import org.springframework.web.bind.annotation.ResponseBody;

 import com.cafecave.api.repository.TableRepository;
 import com.cafecave.api.model.Tables;

 @Controller
 @RequestMapping(path = "/table")
 public class TablesController {
     @Autowired 
     private TableRepository tableRepository;

     @GetMapping(path = "/all")
     public @ResponseBody Iterable<Tables> getAllTables() {
         return tableRepository.findAll();
     }
    
     @PostMapping(path = "/add")
     public @ResponseBody ResponseEntity<Tables> addNewTable(
         @RequestParam Integer capacity, 
         @RequestParam String status) {

         try {
             Tables tableData = new Tables();
             tableData.setCapacity(capacity);
             tableData.setStatus(status);
             tableRepository.save(tableData);
             System.out.println(status);
             return ResponseEntity.ok(tableData);
         } catch (Exception e) {
             return ResponseEntity.notFound().build();
         }
     }

     @GetMapping(path = "/{id}")
     public @ResponseBody ResponseEntity<Tables> getTable(@PathVariable Integer id) {
         try {
             Tables tableData = tableRepository.findById(id).get();
             return ResponseEntity.ok(tableData);
         } catch (Exception e) {
             return ResponseEntity.notFound().build();
         }
     }

     @PutMapping(path = "/{id}")
     public @ResponseBody ResponseEntity<Tables> updateTable(
         @RequestParam Integer capacity, 
         @RequestParam String status, 
         @PathVariable Integer id) {
         try {
             Tables tableData = new Tables();
             tableData.setId(id);
             tableData.setCapacity(capacity);
             tableData.setStatus(status);
             tableRepository.save(tableData);
             return ResponseEntity.ok(tableData);
         } catch (Exception e) {
             return ResponseEntity.notFound().build();
         }
     }
    
     @DeleteMapping(path = "/{id}")
     public @ResponseBody ResponseEntity<Void> deleteTables(@PathVariable int id) {
         try {
             tableRepository.deleteById(id);
             return ResponseEntity.ok().build();
         } catch (Exception e) {
             return ResponseEntity.notFound().build();
         }
     }
 }