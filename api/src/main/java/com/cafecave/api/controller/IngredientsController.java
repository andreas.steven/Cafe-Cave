package com.cafecave.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cafecave.api.model.Ingredients;
import com.cafecave.api.model.Menu;
import com.cafecave.api.model.Stock;
import com.cafecave.api.repository.IngredientsRepository;
import com.cafecave.api.repository.MenuRepository;
import com.cafecave.api.repository.StockRepository;

@Controller
@RequestMapping(path = "/ingredients")
public class IngredientsController {
    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private StockRepository stockRepository;
    
	@Autowired
    private IngredientsRepository ingredientsRepository;
    
    @GetMapping(path = "/all")
    public @ResponseBody ResponseEntity<Iterable<Ingredients>> getAllIngredients() {
        try {
            return ResponseEntity.ok(ingredientsRepository.findAll());
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
    
    @GetMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Ingredients> getIngredient(@PathVariable Integer id) {
        try {
            Ingredients ingredientsData = ingredientsRepository.findById(id).get();
            return ResponseEntity.ok(ingredientsData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
    
    @PostMapping(path = "/add")
    public @ResponseBody ResponseEntity<Ingredients> addIngredient(
            @RequestParam int menu_id,
            @RequestParam int stock_id,
            @RequestParam int required_stock_qty) {

        try {
        	Ingredients ingredientsData = new Ingredients();
        	Menu menuData = menuRepository.findById(menu_id).get();
        	Stock stockData = stockRepository.findById(stock_id).get();
        	ingredientsData.setMenu(menuData);
        	ingredientsData.setStock(stockData);
        	ingredientsData.setRequired_stock_qty(required_stock_qty);
        	ingredientsRepository.save(ingredientsData);
            return ResponseEntity.ok(ingredientsData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
    @PutMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Ingredients> updateIngredient(
    		@PathVariable int id,
            @RequestParam int menu_id,
            @RequestParam int stock_id,
            @RequestParam int required_stock_qty) {

    	 try {
         	Ingredients ingredientsData = new Ingredients();
         	Menu menuData = menuRepository.findById(menu_id).get();
         	Stock stockData = stockRepository.findById(stock_id).get();
         	ingredientsData.setId(id);
         	ingredientsData.setMenu(menuData);
         	ingredientsData.setStock(stockData);
         	ingredientsData.setRequired_stock_qty(required_stock_qty);
         	ingredientsRepository.save(ingredientsData);
             return ResponseEntity.ok(ingredientsData);
         } catch (Exception e) {
             return ResponseEntity.notFound().build();
         }
    }

    @DeleteMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Void> deleteIngredient(@PathVariable int id) {
        try {
            ingredientsRepository.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
