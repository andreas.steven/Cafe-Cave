package com.cafecave.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cafecave.api.model.History;
import com.cafecave.api.repository.HistoryRepository;

@Controller
@RequestMapping(path = "/history")
public class HistoryController {
   @Autowired
   private HistoryRepository historyRepository;

   @GetMapping(path = "/all")
   public @ResponseBody Iterable<History> getAllHistory() {
       return historyRepository.findAll();
   }

   @GetMapping(path = "/{id}")
   public @ResponseBody ResponseEntity<History> getTables(@PathVariable Integer id) {
       try {
           History historyData = historyRepository.findById(id).get();
           return ResponseEntity.ok(historyData);
       } catch (Exception e) {
           return ResponseEntity.notFound().build();
       }
   }
}
