 package com.cafecave.api.controller;

 import java.sql.Date;

 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.http.ResponseEntity;
 import org.springframework.stereotype.Controller;
 import org.springframework.web.bind.annotation.GetMapping;
 import org.springframework.web.bind.annotation.PathVariable;
 import org.springframework.web.bind.annotation.PostMapping;
 import org.springframework.web.bind.annotation.PutMapping;
 import org.springframework.web.bind.annotation.RequestMapping;
 import org.springframework.web.bind.annotation.RequestParam;
 import org.springframework.web.bind.annotation.ResponseBody;

 import com.cafecave.api.model.Stock;
 import com.cafecave.api.repository.StockRepository;

 @Controller
 @RequestMapping(path = "/stock")
 public class StockController {
     @Autowired
     private StockRepository stockRepository;

     @PostMapping(path = "/add")
     public @ResponseBody ResponseEntity<Stock> addNewStock(
         @RequestParam String name, 
         @RequestParam Integer stock_qty,
         @RequestParam Date exp_date) {
                                                                 
         try {
             Stock stockData = new Stock();
             stockData.setName(name);
             stockData.setStock_qty(stock_qty);
             stockData.setExp_date(exp_date);
             stockRepository.save(stockData);
             return ResponseEntity.ok(stockData);
         } catch (Exception e) {
             return ResponseEntity.notFound().build();
         }
     }

     @GetMapping(path = "/all")
     public @ResponseBody ResponseEntity<Iterable<Stock>> getAllStocks() {
         try {
             return ResponseEntity.ok(stockRepository.findAll());
         } catch (Exception e) {
             return ResponseEntity.notFound().build();
         }
     }

     @GetMapping(path = "/{id}")
     public @ResponseBody ResponseEntity<Stock> getStock(@PathVariable int id) {
         try {
             Stock stockData = stockRepository.findById(id).get();
             return ResponseEntity.ok(stockData);
         } catch (Exception e) {
             return ResponseEntity.notFound().build();
         }
     }
 
     @PutMapping(path = "/{id}")
     public @ResponseBody ResponseEntity<Stock> updateStock(
         @RequestParam String name
         ,@RequestParam Integer stock_qty
         ,@RequestParam Date exp_date,
             @PathVariable int id) {
         try {
             Stock stockData = new Stock();
             stockData.setId(id);
             stockData.setName(name);
             stockData.setStock_qty(stock_qty);
             stockData.setExp_date(exp_date);
             stockRepository.save(stockData);
             return ResponseEntity.ok(stockData);
         } catch (Exception e) {
             return ResponseEntity.notFound().build();
         }
     }
 }