package com.cafecave.api.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cafecave.api.model.Discount;
import com.cafecave.api.repository.DiscountRepository;

@Controller
@RequestMapping(path = "/discount")
public class DiscountController {
    @Autowired
    private DiscountRepository discountRepository;

    @PostMapping(path = "/add")
    public @ResponseBody ResponseEntity<Discount> addNewDiscount(
        @RequestParam float rate, 
        @RequestParam Date int_date, 
        @RequestParam Date end_date) {

        try {
            Discount discountData = new Discount();
            discountData.setRate(rate);
            discountData.setInt_date(int_date);
            discountData.setEnd_date(end_date);
            discountRepository.save(discountData);
            return ResponseEntity.ok(discountData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/all")
    public @ResponseBody ResponseEntity<Iterable<Discount>> getAllDiscounts() {
        try {
            return ResponseEntity.ok(discountRepository.findAll());
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Discount> getDiscount(@PathVariable int id) {
        try {
            Discount discountData = discountRepository.findById(id).get();
            return ResponseEntity.ok(discountData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Discount> updateDiscount(
        @RequestParam float rate, 
        @RequestParam Date int_date, 
        @RequestParam Date end_date, 
        @PathVariable int id) {
        try {
            Discount discountData = new Discount();
            discountData.setId(id);
            discountData.setRate(rate);
            discountData.setInt_date(int_date);
            discountData.setEnd_date(end_date);
            discountRepository.save(discountData);
            return ResponseEntity.ok(discountData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
    
    @DeleteMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Void> deleteDiscount(@PathVariable int id){
        try {
            discountRepository.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}