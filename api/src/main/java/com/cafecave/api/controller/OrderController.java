package com.cafecave.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cafecave.api.model.Order;
import com.cafecave.api.model.Discount;
import com.cafecave.api.model.History;
import com.cafecave.api.repository.OrderRepository;
import com.cafecave.api.repository.DiscountRepository;
import com.cafecave.api.repository.HistoryRepository;

import java.sql.Date;

@Controller
@RequestMapping(path = "/order")
public class OrderController {
	@Autowired
    private OrderRepository orderRepository;

    @Autowired
    private DiscountRepository discountRepository;

    @Autowired
    private HistoryRepository historyRepository;
	
    @PostMapping(path = "/add")
    public @ResponseBody ResponseEntity<Order> addNewOrder(
            @RequestParam int id,
            @RequestParam int disc_id,
            @RequestParam Date dateOrdered) {

        try {
            Order orderData = new Order();
            History historyData = new History();
            Discount discData = discountRepository.findById(disc_id).get();
            orderData.setId(id);
            orderData.setDiscount(discData);
            orderData.setDate(dateOrdered);
            orderData.setTotal(0);
            orderRepository.save(orderData);
            historyData.setOrder(orderData); // Save data history saat CREATE Order
            historyRepository.save(historyData);
            return ResponseEntity.ok(orderData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/all")
    public @ResponseBody ResponseEntity<Iterable<Order>> getAllOrders() {
        try {
            return ResponseEntity.ok(orderRepository.findAll());
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Order> getOrder(@PathVariable int id) {
        try {
            Order orderData = orderRepository.findById(id).get();
            return ResponseEntity.ok(orderData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Order> updateOrder(
    		 @RequestParam int disc_id,
             @RequestParam Date date,
             @RequestParam float total,
            @PathVariable int id) {
        try {
             Order orderData = new Order();
             orderData.setId(id);
             Discount discData = discountRepository.findById(disc_id).get();
             orderData.setDiscount(discData);
             orderData.setDate(date);
             orderData.setTotal(total);
             orderRepository.save(orderData);
             return ResponseEntity.ok(orderData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
