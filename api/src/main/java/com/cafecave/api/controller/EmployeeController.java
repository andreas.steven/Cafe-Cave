package com.cafecave.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cafecave.api.model.Employee;
import com.cafecave.api.repository.EmployeeRepository;

@Controller
@RequestMapping(path = "/employee")
public class EmployeeController {
    @Autowired
    private EmployeeRepository employeeRepository;

    @PostMapping(path = "/add")
    public @ResponseBody ResponseEntity<Employee> addNewEmployee(
            @RequestParam String fullname,
            @RequestParam String username,
            @RequestParam int hours,
            @RequestParam String position,
            @RequestParam String password) {

        try {
            Employee employeeData = new Employee();
            employeeData.setFullname(fullname);
            employeeData.setUsername(username);
            employeeData.setHours(hours);
            employeeData.setPosition(position);
            employeeData.setStatus("active");
            employeeData.setPassword(password);
            employeeRepository.save(employeeData);
            return ResponseEntity.ok(employeeData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/all")
    public @ResponseBody ResponseEntity<Iterable<Employee>> getAllEmployees() {
        try {
            return ResponseEntity.ok(employeeRepository.findAll());
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Employee> getEmployee(@PathVariable int id) {
        try {
            Employee employeeData = employeeRepository.findById(id).get();
            return ResponseEntity.ok(employeeData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Employee> updateEmployee(
        @RequestParam String fullname,
        @RequestParam String username, 
        @RequestParam int hours, 
        @RequestParam String position,
        @RequestParam String password, 
        @PathVariable int id) {
        try {
            Employee employeeData = employeeRepository.findById(id).get();
            employeeData.setId(id);
            employeeData.setFullname(fullname);
            employeeData.setUsername(username);
            employeeData.setHours(hours);
            employeeData.setPosition(position);
            employeeData.setStatus(employeeData.getStatus());
            employeeData.setPassword(password);
            employeeRepository.save(employeeData);
            return ResponseEntity.ok(employeeData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(path = "/sack/{id}")
    public @ResponseBody ResponseEntity<Employee> sackEmployee(@PathVariable int id){
        try {
            Employee employeeData = employeeRepository.findById(id).get();
            employeeData.setId(id);
            employeeData.setStatus("inactive");
            employeeRepository.save(employeeData);
            return ResponseEntity.ok(employeeData);
        } catch (Exception e) {
            return ResponseEntity.noContent().build();
        }
    }
}