package com.cafecave.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Set;

import com.cafecave.api.model.Employee;
import com.cafecave.api.model.Ingredients;
import com.cafecave.api.model.Menu;
import com.cafecave.api.repository.EmployeeRepository;
import com.cafecave.api.repository.IngredientsRepository;
import com.cafecave.api.repository.MenuRepository;

@Controller
@RequestMapping(path = "/menu")
public class MenuController {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private MenuRepository menuRepository;
    
    @Autowired
    private IngredientsRepository ingredientRepository;

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<Menu> getAllMenu() {
        return menuRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Menu> getMenu(@PathVariable Integer id) {
        try {
            Menu menuData = menuRepository.findById(id).get();
            return ResponseEntity.ok(menuData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(path = "/add")
    public @ResponseBody ResponseEntity<Menu> addMenu(
            @RequestParam int createdBy,
            @RequestParam String name,
            @RequestParam int price,
            @RequestParam String type,
            @RequestParam String memo) {

        try {
            Menu menuData = new Menu();
            Employee employeeData = employeeRepository.findById(createdBy).get();
            menuData.setCreatedBy(employeeData);
            menuData.setName(name);
            menuData.setPrice(price);
            menuData.setType(type);
            menuData.setMemo(memo);
            menuRepository.save(menuData);
            return ResponseEntity.ok(menuData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Menu> updateMenu(
            @PathVariable int id,
            @RequestParam int createdBy,
            @RequestParam String name,
            @RequestParam float price,
            @RequestParam String type,
            @RequestParam String memo
            ) {

    	try {
            Menu menuData = new Menu();
            Employee employeeData = employeeRepository.findById(createdBy).get();
            menuData.setCreatedBy(employeeData);
            menuData.setId(id);
            menuData.setName(name);
            menuData.setPrice(price);
            menuData.setType(type);
            menuData.setMemo(memo);
            menuRepository.save(menuData);
            return ResponseEntity.ok(menuData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Void> deleteMenu(@PathVariable int id) {
        try {
            // Cari seluruh resep yang terhubung dengan menu yang dipilih
            Set<Ingredients> ingredients =  menuRepository.findById(id).get().getIngredients();
            // Apabila ketemu, hapus seluruh resep yang terhubung
            for(Ingredients ingredient : ingredients){
                ingredientRepository.deleteById(ingredient.getId());
            }
            // Kemudian hapus menu yang telah dipilih
            menuRepository.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}