package com.cafecave.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Date;
import java.util.List;
import java.util.ArrayList;

import com.cafecave.api.model.Employee;
import com.cafecave.api.repository.EmployeeRepository;
import com.cafecave.api.model.Tables;
import com.cafecave.api.repository.TableRepository;
import com.cafecave.api.model.Reservation;
import com.cafecave.api.repository.ReservationRepository;
import com.cafecave.api.dto.Reservation_dto;

@Controller
@RequestMapping(path = "/reservation")
public class ReservationController {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TableRepository tableRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    @GetMapping(path = "/all")
    public @ResponseBody Iterable<Reservation> getAllReservations() {
        return reservationRepository.findAll();
    }

    private ArrayList<Reservation_dto> getReservedTables(int table_id, Date date) {
        List<Reservation> reservations = reservationRepository.findByDate(date);
        ArrayList<Reservation_dto> reserv_data = new ArrayList<>();

        for (Reservation resv : reservations) {
            if (resv.getTable().getId() == table_id) {
                Reservation_dto temp = new Reservation_dto(resv.getTable().getId(), resv.getDate(), resv.getEmail());
                reserv_data.add(temp);
            }
        }
        return reserv_data;
    }

    @GetMapping(path = "/check-reserved-table/{table_id}")
    public @ResponseBody ResponseEntity<List<Reservation_dto>> getReservedData(
        @PathVariable int table_id,
        @RequestParam Date date) {

        try {
            return ResponseEntity.ok( this.getReservedTables(table_id, date) );
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Reservation> getReservation(@PathVariable Integer id) {
        try {
            Reservation reservationData = reservationRepository.findById(id).get();
            return ResponseEntity.ok(reservationData);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(path = "/add")
    public @ResponseBody ResponseEntity<Reservation> addReservation(
            @RequestParam int emp_id,
            @RequestParam int table_id,
            @RequestParam Date date,
            @RequestParam String email) {

        try {
            Reservation reservationData = new Reservation();
            Employee employeeData = employeeRepository.findById(emp_id).get();
            reservationData.setEmployee(employeeData);
            Tables tableData = tableRepository.findById(table_id).get();
            reservationData.setTable(tableData);
            reservationData.setDate(date);
            reservationData.setEmail(email);

            // Cek apakah ada meja yang sudah dibook pada hari yang sama
            ArrayList<Reservation_dto> reservedData = getReservedTables(tableData.getId(), reservationData.getDate());
            if ( reservedData.size() > 0 ){
                return ResponseEntity.noContent().build(); // Apabila meja sudah dipesan, maka tidak bisa dipesan lagi
            } else {
                reservationRepository.save(reservationData);
                return ResponseEntity.ok(reservationData);
            }
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(path = "/{id}")
    public @ResponseBody ResponseEntity<Reservation> updateReservation(
            @PathVariable int id,
            @RequestParam int emp_id,
            @RequestParam int table_id,
            @RequestParam Date date,
            @RequestParam String email) {

        try {
            Reservation reservationData = new Reservation();
            reservationData.setId(id);
            Employee employeeData = employeeRepository.findById(emp_id).get();
            reservationData.setEmployee(employeeData);
            Tables tableData = tableRepository.findById(table_id).get();
            reservationData.setTable(tableData);
            reservationData.setDate(date);
            reservationData.setEmail(email);

            // Cek apakah ada meja yang sudah dibook pada hari yang sama
            ArrayList<Reservation_dto> reservedData = getReservedTables(tableData.getId(), reservationData.getDate());
            if (reservedData.size() > 0) {
                return ResponseEntity.noContent().build(); // Apabila meja sudah dipesan, maka tidak bisa dipesan lagi
            } else {
                reservationRepository.save(reservationData);
                return ResponseEntity.ok(reservationData);
            }
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}