 package com.cafecave.api.model;

 import javax.persistence.Entity;
 import javax.persistence.GeneratedValue;
 import javax.persistence.GenerationType;
 import javax.persistence.Id;
 import javax.persistence.JoinColumn;
 import javax.persistence.ManyToOne;
 import javax.persistence.FetchType;
 import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

 @Entity
 @Table(name = "ingredients")
 public class Ingredients {
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     private int id;

     private int required_stock_qty;

     @ManyToOne(fetch = FetchType.LAZY)
     @JoinColumn(name = "stock_id", nullable = false)
     private Stock stock;

     @ManyToOne(fetch = FetchType.LAZY)
     @JoinColumn(name = "menu_id", nullable = false)
     private Menu menu;

     public int getId() {
         return id;
     }

     public void setId(int id) {
         this.id = id;
     }

     public int getRequired_stock_qty() {
         return required_stock_qty;
     }

     public void setRequired_stock_qty(int required_stock_qty) {
         this.required_stock_qty = required_stock_qty;
     }
     @JsonManagedReference
     public Menu getMenu() {
         return menu;
     }
     
     public void setMenu(Menu menu) {
         this.menu = menu;
     }
     @JsonManagedReference
     public Stock getStock() {
         return stock;
     }

     public void setStock(Stock stock) {
         this.stock = stock;
     }
 }