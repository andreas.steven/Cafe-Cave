package com.cafecave.api.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;

@Entity
@Table(name = "discount")
public class Discount implements Serializable{

    private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "rate")
	private Float rate;
	
	@Column(name = "int_date")
	private Date int_date;
	
	@Column(name = "end_date")
    private Date end_date;
    
    @OneToMany
    (
        targetEntity = Order.class,
        mappedBy = "discount",
        fetch = FetchType.LAZY
    )
    @JsonIgnore
    private Set<Order> orders;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public Date getInt_date() {
		return int_date;
	}

	public void setInt_date(Date int_date) {
		this.int_date = int_date;
	}

	public Date getEnp_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public Date getEnd_date() {
        return end_date;
    }

    @JsonBackReference
    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
        for(Order order : orders){
            order.setDiscount(this);
        }
    }
}