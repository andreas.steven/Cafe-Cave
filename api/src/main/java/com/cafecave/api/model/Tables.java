package com.cafecave.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name="tables")
public class Tables implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "capacity")
    private Integer capacity;
    
    @Column(name = "status")
    private String status;
    
    @OneToMany(
        targetEntity = Reservation.class, 
        mappedBy = "table", 
        orphanRemoval = false, 
        fetch = FetchType.LAZY
    )
    @JsonIgnore
    private Set<Reservation> reservations;

    public Integer getId(){
        return this.id;
    }

    public void setId(Integer id){
        this.id = id;
    }
    
    public Integer getCapacity(){
        return this.capacity;
    }
    
    public void setCapacity(Integer capacity){
        this.capacity = capacity;
    }
    
    public String getStatus(){
        return this.status;
    }
    
    public void setStatus(String status){
        this.status = status;
    }

    @JsonBackReference
    public Set<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(Set<Reservation> reservations) {
        this.reservations = reservations;
        for (Reservation reservation : reservations) {
            reservation.setTable(this);
        }
    }
}