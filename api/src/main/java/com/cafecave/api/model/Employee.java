package com.cafecave.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.FetchType;

import java.io.Serializable;
import java.util.Set;

@Entity
public class Employee implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "username")
    private String username;
    
    @Column(name = "hours")
    private int hours;
    
    @Column(name = "position")
    private String position;
    
    @Column(name = "status")
    private String status;
    
    @Column(name = "password")
    private String password;

    /**
     * Variabel hasil OneToMany dengan tabel 'menu'
     */ 
    @OneToMany
    (
        targetEntity = Menu.class,
        mappedBy = "employee",
        fetch = FetchType.LAZY
    )
    private Set<Menu> menus; // tabel yang punya FKnya adalah tabel 'menu'

    /**
     * Variabel hasil OneToMany dengan table 'reservation'
     */
    @OneToMany
    (
        targetEntity = Reservation.class,
        mappedBy = "employee",
        fetch = FetchType.LAZY
    )
    @JsonIgnore
    private Set<Reservation> reservations;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonBackReference
    public Set<Menu> getMenus() {
        return menus;
    }

    public void setMenus(Set<Menu> menus){
        this.menus = menus;
        for(Menu menu : menus){
            menu.setCreatedBy(this);
        }
    }

    @JsonBackReference
    public Set<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(Set<Reservation> reservations){
        this.reservations = reservations;
        for(Reservation reservation : reservations){
            reservation.setEmployee(this);
        }
    }
}