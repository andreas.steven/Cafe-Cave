package com.cafecave.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.ManyToOne;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

@Entity
@Table(name = "orders")
public class Order implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    /**
     * Variabel hasil relasi ManyToOne dengan tabel 'discount'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "disc_id", nullable = true)
    private Discount discount;

    @Column(name = "date_ordered")
    private Date date_ordered;

    @Column(name = "total")
    private float total;

    /**
     * Variabel hasil relasi OneTOMany dengan tabel 'menu_order'
     */
    @OneToMany(
        targetEntity = MenuOrder.class,
        mappedBy = "order",
        orphanRemoval = false,
        fetch = FetchType.LAZY
    )
    @JsonIgnore
    private Set<MenuOrder> menu_orders;

    /**
     * Variabel hasil relaso OneToMany dengan tabel 'history'
     */
    @OneToMany(
        targetEntity = History.class,
        mappedBy = "order",
        orphanRemoval = false,
        fetch = FetchType.LAZY
    )
    @JsonIgnore
    private Set<History> histories;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonManagedReference
    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public Date getDate() {
        return date_ordered;
    }

    public void setDate(Date date_ordered) {
        this.date_ordered = date_ordered;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    @JsonBackReference
    public Set<MenuOrder> getMenu_orders() {
        return menu_orders;
    }

    public void setMenu_orders(Set<MenuOrder> menu_orders) {
        this.menu_orders = menu_orders;
        for (MenuOrder menu_order : menu_orders){
            menu_order.setOrder(this);
        }
    }

    @JsonBackReference
    public Set<History> getHistories() {
        return histories;
    }

    public void setHistories(Set<History> histories) {
        this.histories = histories;
        for (History history : histories) {
            history.setOrder(this);
        }
    }
}