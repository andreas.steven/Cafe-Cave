package com.cafecave.api.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "stock")
public class Stock implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private Integer stock_qty;
	private Date exp_date;

	/**
	 * Variabel hasil relasi OneToMany dengan tabel 'ingredients'
	 */
	@OneToMany(targetEntity = Ingredients.class, mappedBy = "stock", orphanRemoval = false, fetch = FetchType.LAZY)
	@JsonIgnore
	private Set<Ingredients> ingredients;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStock_qty() {
		return stock_qty;
	}

	public void setStock_qty(Integer stock_qty) {
		this.stock_qty = stock_qty;
	}

	public Date getExp_date() {
		return exp_date;
	}

	public void setExp_date(Date exp_date) {
		this.exp_date = exp_date;
	}

	@JsonBackReference
	public Set<Ingredients> getIngredients() {
		return ingredients;
	}

	public void setIngredients(Set<Ingredients> ingredients) {
		this.ingredients = ingredients;
		for (Ingredients ingredient : ingredients) {
			ingredient.setStock(this);
		}
	}

}