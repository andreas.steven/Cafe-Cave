package com.cafecave.api.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.ManyToOne;

import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Variabel hasil relasi ManyToOne dengan tabel 'employee'
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "createdBy")
    private Employee employee;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private float price;

    @Column(name = "type")
    private String type;

    @Column(name = "memo")
    private String memo;

    /**
     * Variabel hasil relasi OneToMany dengan tabel 'ingredients'
     */
    @OneToMany
    (
        targetEntity = Ingredients.class, 
        mappedBy = "menu", 
        fetch = FetchType.LAZY
    )
    @JsonIgnore
    private Set<Ingredients> ingredients;

    /**
     * Variabel hasil relasi OneToMany dengan tabel 'MenuOrder'
     */
    @OneToMany
    (
        targetEntity = MenuOrder.class,
        mappedBy = "menu",
        fetch = FetchType.LAZY
    )
    @JsonIgnore
    private Set<MenuOrder> menu_orders;

    /**
     * Variabel hasil relasi OneToMany dengan tabel 'Review'
     */
    @OneToMany
    (
        targetEntity = Review.class,
        mappedBy = "menu",
        fetch = FetchType.LAZY
    )
    private Set<Review> reviews;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Employee getCreatedBy() {
        return employee;
    }

    @JsonManagedReference
    public void setCreatedBy(Employee createdBy) {
        this.employee = createdBy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @JsonBackReference
    public Set<Ingredients> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<Ingredients> ingredients) {
        this.ingredients = ingredients;
        for (Ingredients ingredient : ingredients) {
            ingredient.setMenu(this);
        }
    }

    @JsonBackReference
    public Set<MenuOrder> getMenu_order() {
        return menu_orders;
    }

    public void setMenu_order(Set<MenuOrder> menu_orders) {
        this.menu_orders = menu_orders;
        for (MenuOrder menu_order : menu_orders) {
            menu_order.setMenu(this);
        }
    }

    @JsonBackReference
    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
        for (Review review : reviews) {
            review.setMenu(this);
        }
    }
}