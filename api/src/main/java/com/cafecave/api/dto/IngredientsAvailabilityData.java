package com.cafecave.api.dto;

public class IngredientsAvailabilityData {
    private String stockName;
    private int available;
    private int required;
    private boolean is_available;

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getRequired() {
        return required;
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public boolean getIs_available() {
        return is_available;
    }

    public void setIs_available(boolean is_available) {
        this.is_available = is_available;
    }

    public IngredientsAvailabilityData(){
        super();
    }

    public IngredientsAvailabilityData(String stockName, int avaailable, int required, boolean is_available){
        setStockName(stockName);
        setAvailable(available);
        setRequired(required);
        setIs_available(is_available);
    }
}