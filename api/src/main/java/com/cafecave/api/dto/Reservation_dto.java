package com.cafecave.api.dto;

import java.sql.Date;

public class Reservation_dto {
    private int tableNumber;
    private Date dateReserved;
    private String emailReservee;
    
    public int getTableNumber() {
        return tableNumber;
    }
    
    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }
    
    public Date getDateReserved() {
        return dateReserved;
    }
    
    public void setDateReserved(Date dateReserved) {
        this.dateReserved = dateReserved;
    }
    
    public String getEmailReservee() {
        return emailReservee;
    }
    
    public void setEmailReservee(String emailReservee) {
        this.emailReservee = emailReservee;
    }

    public Reservation_dto(int tableNumber, Date dateReserved, String emailReservee){
        setTableNumber(tableNumber);
        setDateReserved(dateReserved);
        setEmailReservee(emailReservee);
    }
}