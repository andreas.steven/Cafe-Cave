@extends('layout/master')
@section('title','View Reservation')
@section('judul','View Reservation')
@section('container')

<section class="ftco-intro">
    <div class="container-wrap">
        <div class="wrap d-md-flex align-items-xl-end">
            <div class="view-reserve">
                <div class="row no-gutters">
                    <table class="table">
                    <thead class="thead-primary">
                      <tr class="text-center">
                        <th>Fullname</th>
                        <th>Phone</th>
                        <th>E-mail</th>
                        <th>Time</th>
                        <th>Date</th>
                        <th>Approve</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="text-center">
                        <td class="customer-name">
                            <h3></h3>
                            <p></p>
                        </td>
                        <td class="phone"></td>
                        <td class="email"></td>
                        <td class="time"></td>
                        <td class="date"></td>
                        <td>
                        <a href="" class='badge badge-success'>Apporve</a>
                        </td>
                      </tr><!-- END TR-->
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection