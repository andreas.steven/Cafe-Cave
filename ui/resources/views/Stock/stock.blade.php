@extends('layout.admin')
@section('title','Stock')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">List Stock</h1></center>        
                  <a href ="/createStock" > +ADD  </a>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Name</th>
                            <th scope='col'>Stock</th>
                            <th scope='col'>Expired Date</th>
                            <th scope='col'>Aksi</th>
                        </tr> 
                    </thead>
                    <tbody>
                        @foreach ( $liststock as $stock )
                        <tr>
                            <td>{{ $stock->name }}</a></td>
                            <td>{{ $stock->stock_qty }}</td>
                            <td>{{ $stock->exp_date }}</td>
                            <td>
                                <a href="/stock/{{ $stock->id }}/edit" class='badge badge-success'>EDIT</a>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection