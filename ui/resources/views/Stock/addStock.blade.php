@extends('layout.admin')
@section('title','Create Stock')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Create Stock</h1></center>        

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Name</th>
                            <th scope='col'>Stock</th>
                            <th scope='col'>Expired Date</th>
                            <th scope='col'>Aksi</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/stock" method='post'>
                            <td><input type="text" name='name' placeholder="Input Name"></td>
                            <td><input type="number" name='qty' placeholder="Input Stock"></td>
                            <td><input type="date" name='expired' placeholder="Input Expired Date"></td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='CREATE'>
                            </td>
                            {{ csrf_field() }}
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection