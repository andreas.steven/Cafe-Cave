@extends('layout.admin')
@section('title','Edit Stock')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Edit Stock</h1></center>        

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Name</th>
                            <th scope='col'>Stock</th>
                            <th scope='col'>Expired Date</th>
                            <th scope='col'>Aksi</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/stock/{{$detailstock->id}}" method='post'>
                            <td><input type="text" name='name' value="{{$detailstock->name}}"></td>
                            <td><input type="number" name='qty' value="{{$detailstock->stock_qty}}"></td>
                            <td><input type="date" name='expired' value="{{$detailstock->exp_date}}"></td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='EDIT'>
                            </td>
                            {{ csrf_field() }}
                            <input type="hidden" name='_method' value='PUT'>
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection