@extends('layout/master')
@section('title','Reservation')
@section('judul','Reservation')
@section('container')

<section class="ftco-intro">
    <div class="container-wrap">
        <div class="wrap d-md-flex align-items-xl-end">
            <div class="info">
                <div class="row no-gutters">
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="icon"><span class="icon-phone"></span></div>
                        <div class="text">
                            <h3>000 (123) 456 7890</h3>
                            <p>A small river named Duden flows by their place and supplies.</p>
                        </div>
                    </div>  
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="icon"><span class="icon-my_location"></span></div>
                        <div class="text">
                            <h3>198 West 21th Street</h3>
                            <p>	203 Fake St. Mountain View, San Francisco, California, USA</p>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="icon"><span class="icon-clock-o"></span></div>
                        <div class="text">
                            <h3>Open Monday-Friday</h3>
                            <p>8:00am - 9:00pm</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="book p-4">
	    			<h3>Book a Table</h3>
	    			<form action="/reserve" method="post">
	    				<div class="d-md-flex">
		    				<div class="form-group">
		    					<input name="firstname" type="text" placeholder="First Name"></input>
		    				</div>
		    				<div class="form-group ml-md-4">
		    					<input name="lastname" type="text" placeholder="Last Name"></input>
		    				</div>
	    				</div>
	    				<div class="d-md-flex">
		    				<div class="form-group">
		    					<div class="input-wrap">
		    						<input name="phone" type="text" placeholder="Phone"></input>
	            				</div>
		    				</div>
		    				<div class="form-group ml-md-4">
		    					<div class="input-wrap">
		            				<div class="icon"><span class="ion-ios-clock"></span></div>
		            				<input name="time" type="text" laceholder="Time"></input>
	            				</div>
		    				</div>
		    				<div class="form-group ml-md-4">
		    					<div class="icon"><span class="ion-md-calendar"></span></div>
		            			<input name="date" type="text" placeholder="Date"></input>
		    				</div>
	    				</div>
	    				<div class="d-md-flex">
	    					<div class="form-group">
		           			   <input type="email" name="email" id="" cols="30" rows="2" class="form-control" placeholder="E-mail"></input>
		           		 	</div>
				           <div class="form-group ml-md-4">
				              <input type="submit" value="Appointment" name="sumbit"></input>
				           </div>	
				        </div>
				           {{ csrf_field() }} 
	    			</form>
	    		</div>
        </div>
    </div>
</section>

@endsection