@extends('layout.admin')
@section('title','Edit Discount')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Edit Discount</h1></center>        

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Rate</th>
                            <th scope='col'>Start Date</th>
                            <th scope='col'>End Date</th>
                            <th scope='col'>Action</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/discount/{{$detaildiscount->id}}" method='post'>
                            <td><input type="number" name='rate' step='0.01' value="{{$detaildiscount->rate}}"></td>
                            <td><input type="date" name='init_date' value="{{$detaildiscount->init_date}}"></td>
                            <td><input type="date" name='end_date' value="{{$detaildiscount->end_date}}"></td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='EDIT'>
                            </td>
                            {{ csrf_field() }}
                            <input type="hidden" name='_method' value='PUT'>
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection