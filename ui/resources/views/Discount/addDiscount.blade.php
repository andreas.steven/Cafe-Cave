@extends('layout.admin')
@section('title','Create Discount')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Create Discount</h1></center>        

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Rate</th>
                            <th scope='col'>Start Date</th>
                            <th scope='col'>End Date</th>
                            <th scope='col'>Action</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/discount" method='post'>
                            <td><input type="number" step='0.01' name='rate' placeholder="Input Rate"></td>
                            <td><input type="date" name='init_date' placeholder="Input Start Date"></td>
                            <td><input type="date" name='end_date' placeholder="Input Expired Date"></td>
                            <td>
                                <input style="background-color:green; border-radius:25px;" type="submit" name='submit' value='CREATE'>
                            </td>
                            {{ csrf_field() }}
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection