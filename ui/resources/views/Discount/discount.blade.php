@extends('layout.admin')
@section('title','Discount')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">List Discount</h1></center>        
                  <a href ="/createDiscount" > +ADD  </a>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Rate</th>
                            <th scope='col'>Start Date</th>
                            <th scope='col'>End Date</th>
                            <th scope='col'>Action</th>
                        </tr> 
                    </thead>
                    <tbody>
                        @foreach ( $listdiscount as $discount )
                        <tr>
                            <td>{{ $discount->rate }}</td>
                            <td>{{ $discount->init_date }}</td>
                            <td>{{ $discount->end_date }}</td>
                            <td>
                                <a href="/discount/{{ $discount->id }}/edit" class='badge badge-success'>EDIT</a>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection