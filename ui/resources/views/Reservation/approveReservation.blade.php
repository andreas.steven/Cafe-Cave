@extends('layout.admin')
@section('title','Approve Reservation')
@section('judul','Approve Reservation')
@section('container')

<section class="ftco-intro" style="margin-top: 100px">
    <div class="container-wrap">
        <div class="wrap d-md-flex align-items-xl-end">
            <div class="view-reserve">
                <div class="row no-gutters">
                    <table class="table">
                    <thead class="thead-primary">
                      <tr class="text-center">
                        <th>Employee</th>
                        <th>Fullname</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Email</th>
                        <th>Table</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                     <tbody>
                          <tr class="text-center">
                            <form action="/approve/{{$listreserve->id}}/delete" method='post'>
                              <td>{{ Auth::user()->fullname }}</td>
                              <td class="customername">{{ $listreserve->firstname }} &nbsp {{ $listreserve->lastname }} </td>
                              <td class="date">{{ $listreserve->date }}

                              <input type="hidden" name='date' value="{{ $listreserve->date }}"></td>
                              <td class="date">{{ $listreserve->time }}</td>

                              <td class="date" >{{ $listreserve->email }}
                              <input type="hidden" name='email' value="{{ $listreserve->email }}"></td>
                              <td>
                                  <select name="table" style="width: 100px">
                                           @foreach ($listtable as $table)
                                              <option value= "{{$table -> id}}">{{ $table -> id }} - {{ $table -> capacity }}</option>
                                           @endforeach
                                  </select> 
                              </td>

                              <td>
                                       <input style="background-color:green; border-radius:1px; width: 100px;" type="submit" name='submit' value='Approve'>
                                      {{ csrf_field()}}
                                       </input>
                                       <input type="hidden" name="_method" value='DELETE'>
                              </td>
                            </form>
                            </form>
                          </tr><!-- END TR --> 
                    
                    </tbody>
                  </table>

                
                </div>
            </div>
        </div>
    </div>
</section>


@endsection