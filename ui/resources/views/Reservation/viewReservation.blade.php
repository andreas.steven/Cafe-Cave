@extends('layout.admin')
@section('title','View Reservation')
@section('judul','View Reservation')
@section('container')

<section class="ftco-intro" style="margin-top: 100px">
    <div class="container-wrap">
        <div class="wrap d-md-flex align-items-xl-end">
            <div class="view-reserve">
                <div class="row no-gutters">
                    <table class="table">
                    <thead class="thead-primary">
                      <tr class="text-center">
                        <th>Fullname</th>
                        <th>Phone</th>
                        <th>E-mail</th>
                        <th>Time</th>
                        <th>Date</th>
                        <th>Approve</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach( $listreserve as $listReserve )
                          <tr class="text-center">
                                <td class="customer-name">{{ $listReserve->firstname }} &nbsp {{ $listReserve->lastname }} </td>
                                <td class="phone">{{ $listReserve->phone }}</td>
                                <td class="email">{{ $listReserve->email }}</td>
                                <td class="time">{{ $listReserve->time }}</td>
                                <td class="date">{{ $listReserve->date }}</td>
                                 <td>
                                        <a href="/approve/{{ $listReserve-> id}}/edit" class='badge badge-success'>Approve</a>
                                </td>
                          </tr><!-- END TR-->
                        {{ csrf_field() }} 
                        @endforeach
   
                    </tbody>
                  </table>

                </div>
            </div>
        </div>
    </div>
</section>


@endsection