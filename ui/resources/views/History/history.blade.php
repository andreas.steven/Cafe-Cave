@extends('layout.admin')
@section('title','History')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">List History</h1></center>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>History Id</th>
                            <th scope='col'>Order Id</th>
                            <th scope='col'>Order Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $listquery as $query )
                        <tr>
                            <td>{{ $query->id }}</td>
                            <td>{{ $query->order_id }}</a></td>
                            <td>{{ $query->date }}</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection
