@extends('layout/admin')
@section('title','Ingredients')
@section('judul','Ingredients')
@section('container')

    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">List Ingredients</h1></center>        
                  <a href ="/createIngredient" > +ADD  </a>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Menu Name</th>
                            <th scope='col'>Stock Name</th>
                            <th scope='col'>Required Stock Quantity</th>
                            <th scope='col'>Action</th>
                        </tr> 
                    </thead>    
                    <tbody>
                        @foreach ( $listingredients as $ingredient )
                        <tr>
                            <td>{{ $ingredient->menu_id }}</td>
                            <td>{{ $ingredient->stock_id }}</td>
                            <td>{{ $ingredient->required_stock_qty }}</td>
                            <td>
                                <a href="/ingredient/{{ $ingredient-> id }}/edit" class='badge badge-success'>EDIT</a>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
      </div>

@endsection