@extends('layout.admin')
@section('title','Create Ingredient')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Create Ingredient</h1></center>        

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Menu Name</th>
                            <th scope='col'>Stock Name</th>
                            <th scope='col'>Required Stock Quantity</th>
                            <th scope='col'>Action</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/ingredient" method='post'>
                        <td>
                            <select name="menu" style="width: 100px">
                              @foreach ($listmenu as $menu)
                                 <option value= "{{$menu -> id}}">{{ $menu -> id }} - Rp. {{ $menu -> name }} - {{ $menu -> createdBy }}</option>
                              @endforeach
                            </select>
                            </td><td>
                            <select name="stock" style="width: 100px">
                              @foreach ($liststock as $stock)
                                 <option value= "{{$stock -> id}}">{{ $stock -> id }} - Rp. {{ $stock -> name }}</option>
                              @endforeach
                            </select>
                            </td>
                            <td><input type="number" name='req_stock' placeholder="Input Required Quantity Stock"></td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='CREATE'>
                            </td>
                            {{ csrf_field() }}
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection