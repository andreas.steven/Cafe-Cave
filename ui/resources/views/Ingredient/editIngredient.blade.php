@extends('layout.admin')
@section('title','Edit Ingredient')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Edit Ingredient {{ $detailingredient->id }} </h1></center>        

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                           <th scope='col'>Menu Name</th>
                            <th scope='col'>Stock Name</th>
                            <th scope='col'>Required Stock Quantity</th>
                            <th scope='col'>Action</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/ingredient/{{$detailingredient->id}}" method='post'>
                        <td>
                            <p> {{ $querymenu[0]->name }}  </p>
                            </td><td>
                            <select name="stock" style="width: 100px">
                              @foreach ($liststock as $stock)
                                 <option value= "{{$stock -> id}}">{{ $stock -> id }} - Rp. {{ $stock -> name }}</option>
                              @endforeach
                            </select>
                            </td>
                            <td><input type="number" name='req_stock' placeholder="{{$detailingredient->required_qty_stock}}"></td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='EDIT'>
                            </td>
                            {{ csrf_field() }}
                            <input type="hidden" name='_method' value='PUT'>
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection