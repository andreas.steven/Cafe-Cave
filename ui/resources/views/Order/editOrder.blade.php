@extends('layout.admin')
@section('title','Edit Order')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Edit Order</h1></center>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Order Id</th>
                            <th scope='col'>Discount</th>
                            <th scope='col'>Total</th>
                            <th scope='col'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/order/{{$detailorder->id}}" method='post'>
                            <td style="color: white">{{$detailorder->id}}</td>
                            <td>
                              <select name="discount" style="width: 100px">
                                @foreach ($listdiscount as $discount)
                                     <option value= "{{$discount -> id}}">{{ $discount -> id }} - {{ $discount -> rate }}</option>
                                @endforeach
                              </select>
                            </td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='EDIT'>
                            </td>
                            {{ csrf_field() }}
                            <input type="hidden" name='_method' value='PUT'>
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection
