@extends('layout.master')
@section('title','Create Order')
@section('judul','Create Order')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Create an Order</h1></center>

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Discount</th>
                            <th scope='col'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/order" method='post'>
                            <td>
                            <select name="discount" style="width: 100px">
                              @foreach ($listdiscount as $discount)
                                 <option value= "{{$discount -> id}}">{{ $discount -> id }} - {{ $discount -> rate }}</option>
                              @endforeach
                            </select>
                            </td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='CREATE'>
                            </td>
                            {{ csrf_field() }}
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection
