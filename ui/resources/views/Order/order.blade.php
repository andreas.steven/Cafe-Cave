@extends('layout.admin')
@section('title','Order')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">List Order</h1></center>
                  <a href ="/createOrder" > +ADD  </a>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Order</th>
                            <th scope='col'>Discount</th>
                            <th scope='col'>Date</th>
                            <th scope='col'>Total</th>
                            <th scope='col'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $listorder as $order )
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->disc_id }}</td>
                            <td>{{ $order->date }}</td>
                            <td>{{ $order->total }}</td>
                            <td>
                                <a href="/order/{{ $order->id }}/edit" class='badge badge-success'>EDIT</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
      </div>

@endsection
