@extends('layout.admin')
@section('title','Review')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">List Review </h1></center>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Menu </th>
                            <th scope='col'>Name</th>
                            <th scope='col'>Rating</th>
                            <th scope='col'>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $listreivew as $review )
                        <tr>
                            <td>{{ $review->menu_id }}</td>
                            <td>{{ $review->name }}</td>
                            <td>{{ $review->rating }}</td>
                            <td>{{ $review->description }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
      </div>

@endsection
