@extends('layout/master')
@section('title','Create Review')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Add Review</h1></center>

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Menu</th>
                            <th scope='col'>Name</th>
                            <th scope='col'>Rating</th>
                            <th scope='col'>Description</th>
                            <th scope='col'>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/review" method='post'>
                            <td>
                            <select name="menu" style="width: 100px">
                              @foreach ($listmenu as $menu)
                                 <option value= "{{$menu -> id}}">{{ $menu->id }} - {{ $menu -> name }}</option>
                              @endforeach
                            </select>
                            </td>
                            <td><input type="text" name='name' placeholder="Input Your Name"></td>
                            <td><input type="number" name='rating' placeholder="Input Rating ( 1-10 )"></td> 
                            <td><input type="text" name='description' placeholder="Input Description"></td> 
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='CREATE'>
                            </td>
                            {{ csrf_field() }}
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection
