@extends('layout.admin')
@section('title','Create Menu Order')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Create Menu Order</h1></center>

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Order</th>
                            <th scope='col'>Menu </th>
                            <th scope='col'>Quantity</th>
                            <th scope='col'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/menuorder" method='post'>
                              <td>
                            <select name="order" style="width: 100px">
                              @foreach ($listorder as $order)
                                 <option value= "{{$order -> id}}">{{ $order-> id }}</option>
                              @endforeach
                            </select>
                            </td>
                            <td>
                            <select name="menu" style="width: 100px">
                              @foreach ($listmenu as $menu)
                                 <option value= "{{$menu -> id}}">{{ $menu -> id }} - {{ $menu -> name }}</option>
                              @endforeach
                            </select>
                            </td>
                            <td><input type="number" name='qty' placeholder="Input Quantity Order"></td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='CREATE'>
                            </td>
                            {{ csrf_field() }}
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection
