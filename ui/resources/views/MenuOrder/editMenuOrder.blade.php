@extends('layout.admin')
@section('title','Edit Menu Order')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Edit Menu Order</h1></center>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Order</th>
                            <th scope='col'>Menu </th>
                            <th scope='col'>Quantity</th>
                            <th scope='col'>SubTotal</th>
                            <th scope='col'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/menuorder/{{$detailmenuorder->id}}" method='post'>
                            <td style="color: white">{{$detailmenuorder->id}}</td>
                            <td>
                              <select name="menu" style="width: 100px">
                                @foreach ($listmenu as $menu)
                                     <option value= "{{$menu -> id}}">{{ $menu -> id }} - {{ $menu -> name }}</option>
                                @endforeach
                              </select>
                            </td>
                            <td><input type="number" name='qty' value="{{$detailmenuorder->qty}}"></td>
                            <td><input type="number" name='subtotal' value="{{$detailmenuorder->subtotal}}"></td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='EDIT'>
                            </td>
                            {{ csrf_field() }}
                            <input type="hidden" name='_method' value='PUT'>
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection
