@extends('layout.admin')
@section('title','Menu Order')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">List Menu Order</h1></center>
                  <a href ="/createMenuOrder" > +ADD  </a>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Order</th>
                            <th scope='col'>Menu</th>
                            <th scope='col'>Quantity</th>
                            <th scope='col'>Subtotal</th>
                            <th scope='col'>Azksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $listmenuorder as $menuorder )
                        <tr>
                            <td>{{ $menuorder->order_id }}</td>
                            <td>{{ $menuorder->menu_id }}</td>
                            <td>{{ $menuorder->qty }}</td>
                            <td>{{ $menuorder->subtotal }}</td>
                            <td>
                                <a href="/menuorder/{{ $menuorder->id }}/edit" class='badge badge-success'>EDIT</a>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
      </div>

@endsection
