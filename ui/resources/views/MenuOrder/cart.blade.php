@extends('layout.master')

@section('title', 'Cart')

@section('judul','Cart')

@section('container')
<section class="ftco-section ftco-cart">
    <div class="container">
        <div class="row">
        <div class="col-md-12 ftco-animate">
            <div class="cart-list">
                <table class="table">
                    <thead class="thead-primary">
                      <tr class="text-center">
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach( $user_cart as $cart )
                      <tr class="text-center">
                      
                      <form action="/menuorder/{{ $cart->order_id }}/{{$cart->menu_id}}/delete" method='post'>
                        <!-- <td class="product-remove"><a href="#"><span class="icon-close"></span></a></td> -->
                        <td class="product-remove">
                            <button type="submit" style="background-color:brown; width:45px" class="btn fa fa-close"> </button>
                            {{ csrf_field() }}
                             <input type="hidden" name="_method" value='DELETE'>
                        </td>
                      </form>
                        <td class="image-prod"><div class="img" style="background-image:url({{ $cart->url }});"></div></td>

                        <td class="product-name">
                            <h3>{{$cart->name}} </h3>
                            <p>{{$cart->memo}}</p>
                        </td>

                        <td class="price"> {{$cart->price}} </td>

                        <td class="quantity"> {{$cart->qty}} </td>

                        <td class="total"> {{$cart->subtotal}} </td>
                      </tr><!-- END TR-->
                    @endforeach
                    </tbody>
                  </table>
              </div>
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col col-lg-3 col-md-6 mt-5 cart-wrap ftco-animate">
            <div class="cart-total mb-3">
                @if ( $user_cart->isNotEmpty())
                <a hidden> {{!!$subtotal = 0 !!}}
                    @foreach($user_cart as $cart)
                        {{!!$subtotal += $cart->subtotal; !!}} 
                    @endforeach 
                </a>
                <p class="d-flex">
                    <span>Subtotal</span>
                    <span>{{$subtotal}}</span>
                </p>

                <a hidden> {!!$discount = $user_cart[0]->rate*$subtotal !!} </a>
                <p class="d-flex">
                    <span>Discount</span>
                    <span>{{$discount}}</span>
                </p>
                <hr>
                <a hidden> {!!$total = $subtotal - $discount !!}</a>
                <p class="d-flex total-price">
                    <span>Total</span>
                    <span>{{$total}}</span>
                </p>
                <form action ="/payment/{{ $cart->order_id }}/{{$total}}/payment" method='post' >
                <input type="submit" class="btn btn-primary py-3 px-4"></input>
                {{ csrf_field() }}
                <input type="hidden" name='_method' value='PUT'>
                </form>
                @else
                <p class="d-flex">
                    <span>Subtotal</span>
                    <span>0</span>
                </p>
                <p class="d-flex">
                    <span>Discount</span>
                    <span>0</span>
                </p>
                <hr>
                <p class="d-flex total-price">
                    <span>Total</span>
                    <span>0</span>
                </p>
                @endif
            </div>
        </div>
    </div>
    </div>
</section>


@endsection
