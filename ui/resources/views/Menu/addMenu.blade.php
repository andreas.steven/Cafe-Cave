@extends('layout.admin')
@section('title','Create Menu')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Create Menu</h1></center>        

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Name</th>
                            <th scope='col'>Price</th>
                            <th scope='col'>Type</th>
                            <th scope='col'>Memo</th>
                            <th scope='col'>Image</th>
                            <th scope='col'>Action</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/menu" method='post' enctype="multipart/form-data">
                            <td><input type="text" name='name' placeholder="Input Name"></td>
                            <td><input type="number" name='price' placeholder="Input Price"></td>
                            <td><input type="text" name='type' placeholder="Input Type"></td>
                            <td><input type="text" name='memo' placeholder="Input Memo"></td>
                            <td><input type="file" name="file"></td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='CREATE'>
                            </td>
                            {{ csrf_field() }}
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection