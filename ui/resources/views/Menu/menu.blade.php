@extends('layout.admin')
@section('title','Menu')
@section('container')

    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">List Menu</h1></center>        
                  <a href ="/createMenu" > +ADD  </a>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Created By</th>
                            <th scope='col'>Name</th>
                            <th scope='col'>Price</th>
                            <th scope='col'>Type</th>
                            <th scope='col'>Memo</th>
                            <th scope='col'>Gambar</th>
                            <th scope='col'>Action</th>
                        </tr> 
                    </thead>
                    <tbody>
                        @foreach ( $listmenu as $menu )
                        <tr>
                            <td>{{ $menu->createdBy }}</td>
                            <td>{{ $menu->name }}</td>
                            <td>{{ $menu->price }}</td>
                            <td>{{ $menu->type }}</td>
                            <td>{{ $menu->memo }}</td>
                            <td><img style = 'width:50px;height:50px;'src={{ $menu->url }}></img></td>
                            <td>
                                <a href="/menu/{{ $menu->id }}/edit" class='badge badge-success'>EDIT</a>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection

