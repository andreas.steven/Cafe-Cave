@extends('layout.master')
@section('title','Menu')
@section('judul','Menu')
@section('container')



<section class="ftco-menu mb-5 pb-5">
     <div class="container">
      <div class="row justify-content-center mb-5">
          <div class="col-md-7 heading-section text-center ftco-animate">
           <span class="subheading">Discover</span>
            <h2 class="mb-4">Our Products</h2>
            <h2 class="mb-4" hidden>{{ $id }}</h2>
            <p>Far far away, behindthe word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
          </div>
        </div>
      <div class="row d-md-flex">
       <div class="col-lg-12 ftco-animate p-md-5">
        <div class="row">
            <div class="col-md-12 nav-link-wrap mb-5">
              <div class="nav ftco-animate nav-pills justify-content-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true">Makanan</a>

                <a class="nav-link" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">Minuman</a>

                <a class="nav-link" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false">Dessert</a>
              </div>
            </div>
            <div class="col-md-12 d-flex align-items-center">
              
              <div class="tab-content ftco-animate" id="v-pills-tabContent">

                <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
					<div class="row">
						@foreach( $listmakanan as $menu )
							<div class="col-md-4 text-center">
								<div class="menu-wrap">
									<a href="#" class="menu-img img mb-4" style="background-image: url({{$menu->url}});"></a>
									<div class="text">
										<form action="menuordercust" method="POST">
											<input type="number" name ='menuid' value ="{{$menu->id}}" hidden></input>
											<h3>{{ $menu->name }}</h3>
											<p>{{ $menu->memo }}</p>
											<p name = "price" class="price"><span>IDR - {{ $menu->price }}</span></p>
											<p> <input type='number' name='qty' placeholder="Input Quantity..."> </input> </p>
											<p> <input type='submit' class="btn btn-primary btn-outline-primary" name="submit" value="Add To Cart"></input></p>
											{{ csrf_field() }}
											<!-- <p><a href="#" value="CREATE" class="btn btn-primary btn-outline-primary">Add to cart</a></p> -->
										</form>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>

       <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2-tab">
                  <div class="row">
         @foreach ( $listminuman as $minuman )
                  <div class="col-md-4 text-center" style="width:600px;">
                   <div class="menu-wrap">
                    <a href="#" class="menu-img img mb-4" style="background-image: url({{ $minuman->url }});"></a>
                    <div class="text">
                      <form action="menuordercust" method="POST">
                        <input type="number" name ='menuid' value ="{{$minuman->id}}" hidden></input>
                        <h3>{{ $minuman->name }}</h3>
                        <p> {{$minuman->memo}} </p>
                        <p class="price"><span>IDR -  {{ $minuman->price }} </span></p>
                        <p> <input type='number' name='qty' placeholder="Input Quantity..."> </input> </p>
                        <p> <input type='submit' class="btn btn-primary btn-outline-primary" name="submit" value="Add To Cart"></input></p>
                        {{ csrf_field() }}
                     </form>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
        </div>

						<div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3-tab">
						<div class="row">
			@foreach ($listdessert as $dessert)
					  	<div class="col-md-4 text-center" style="width:600px;">
					  	<div class="menu-wrap">
						  	<a href="#" class="menu-img img mb-4" style="background-image: url({{ $dessert->url }});"></a>
						  	<div class="text">
                   <form action="menuordercust" method="POST">
                   <input type="number" name ='menuid' value ="{{$dessert->id}}" hidden></input>
                    <h3><a href="#">{{$dessert->name}}</a></h3>
                    <p>{{ $dessert->memo }}</p>
                    <p class="price"><span>{{ $dessert->price }}</span></p>
                    <p> <input type='number' name='qty' placeholder="Input Quantity..."> </input> </p>
                    <p> <input type='submit' class="btn btn-primary btn-outline-primary" name="submit" value="Add To Cart"></input></p>
                    {{ csrf_field() }}
                </form>
							</div>
						</div>
					</div>
	      	@endforeach
        </div>
      </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     </div>
    </section>

@endsection