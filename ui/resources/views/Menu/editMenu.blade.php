@extends('layout.admin')
@section('title','Order')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Edit Menu</h1></center>        

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Name</th>
                            <th scope='col'>Price</th>
                            <th scope='col'>Type</th>
                            <th scope='col'>Memo</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/menu/{{$detailmenu->id}}" method='post'>
                            <td><input type="text" name='name' value="{{$detailmenu->name}}"></td>
                            <td><input type="number" name='price' value="{{$detailmenu->price}}"></td>
                            <td><input type="text" name='type' value="{{$detailmenu->type}}"></td>
                            <td><input type="text" name='memo' value="{{$detailmenu->memo}}"></td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='EDIT'>
                            </td>
                            {{ csrf_field() }}
                            <input type="hidden" name='_method' value='PUT'>
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection