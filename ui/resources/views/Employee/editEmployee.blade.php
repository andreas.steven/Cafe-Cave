@extends('layout.admin')
@section('title','Edit Employee')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Edit Status Employee</h1></center>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Full Name </th>
                            <th scope='col'>Status</th>
                            <th scope='col'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <form action="/employee/{{$detailemployee->id}}" method='post'>
                            <td><p> {{ $detailemployee->fullname }} </p></td>
                            <td> <select name="status" style="width: 100px">
                              @foreach ($liststatus as $status)
                                 <option value= "{{ $status }}">{{ $status }}</option>
                              @endforeach
                            </select>
                            </td>
                            <td>
                            <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='EDIT'>
                            </td>
                            {{ csrf_field() }}
                            <input type="hidden" name='_method' value='PUT'>
                        </form>
                        </tr> 
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection

