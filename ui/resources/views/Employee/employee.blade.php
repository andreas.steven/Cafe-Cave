@extends('layout.admin')
@section('title','List Employee')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">List Employee</h1></center>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Full Name </th>
                            <th scope='col'>Status</th>
                            <th scope='col'>Position</th>
                            @if ( $position=='manager' )
                            <th scope='col'>Aksi</th>
                            @else
                            <th> &nbsp; </th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $listemployee as $employee )
                        <tr>
                            <td>{{ $employee->fullname }}</td>
                            <td>{{ $employee->status }}</td>
                            <td>{{ $employee->position }}</td>
                            @if ( $position=='manager' )
                            <td>
                                <a href="/employee/{{ $employee->id }}/edit" class='badge badge-success'>EDIT</a>
                                </form>
                            </td>
                            @else
                            <td>
                                <p> &nbsp;</p>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection
