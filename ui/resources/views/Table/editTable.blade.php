@extends('layout.admin')
@section('title','Edit Table')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Edit Table</h1></center>

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Table Number</th>
                            <th scope='col'>Capacity</th>
                            <th scope='col'>Status</th>
                            <th scope='col'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/table/{{$detailtable->id}}" method='post'>
                            <td><input type="number" name='id' value="{{$detailtable->id}}"></td>
                            <td><input type="number" name='capacity' value="{{$detailtable->capacity}}"></td>
                            <td><input type="text" name='status' value="{{$detailtable->status}}"></td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='EDIT'>
                            </td>
                            {{ csrf_field() }}
                            <input type="hidden" name='_method' value='PUT'>
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection
