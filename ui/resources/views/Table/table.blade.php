@extends('layout.admin')
@section('title','Table')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">List Table</h1></center>
                  <a href ="/createTable" > +ADD  </a>
                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Table Number</th>
                            <th scope='col'>Capacity</th>
                            <th scope='col'>Status</th>
                            <th scope='col'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $listtable as $table )
                        <tr>
                            <td>{{ $table->id }}</td>
                            <td>{{ $table->capacity }}</a></td>
                            <td>{{ $table->status }}</td>
                            <td>
                                <a href="/table/{{ $table->id }}/edit" class='badge badge-success'>EDIT</a>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection
