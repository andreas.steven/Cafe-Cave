@extends('layout.admin')
@section('title','Create Table')
@section('container')
    <div class="container" style="margin-top:100px">
          <div class="row">
              <div class="col-10">
                  <center><h1 class="mt-3">Create a Table</h1></center>

                  <table class='table'>
                    <thead class='thead-dark'>
                        <tr>
                            <th scope='col'>Capacity</th>
                            <th scope='col'>Status</th>
                            <th scope='col'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <form action="/table" method='post'>
                            <td><input type="number" name='capacity' placeholder="Input Table's Capacity"></td>
                            <td><input type="text" name='status' placeholder="Input Table's Status"></td>
                            <td>
                                <input style="background-color:green; border-radius:1px;" type="submit" name='submit' value='CREATE'>
                            </td>
                            {{ csrf_field() }}
                        </form>
                        </tr>
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
@endsection
