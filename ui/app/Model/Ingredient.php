<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $table = "ingredients";
    /*
    Karena ada table Students di DB, maka otomatis
    akan ambil table Students ( karena singular dan plural ) Student ambil Students
    */
    protected $fillable = ['menu_id', 'stock_id','required_stock_qty'];
    // protected $guarded = ['id'];
    /*
    Kalau lebih banyak yg fillable, pake guarded
    kalau lebih banyak guarded, pake fillable
    biar simple + sedikit tulisnya
    */
    public $timestamps = false;
}
