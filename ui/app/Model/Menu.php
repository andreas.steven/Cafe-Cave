<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class menu extends Model
{
    protected $table = "menu";
    /*
    Karena ada table Students di DB, maka otomatis
    akan ambil table Students ( karena singular dan plural ) Student ambil Students
    */
    // protected $fillable = ['id'];
    protected $guarded = ['id'];
    /*
    Kalau lebih banyak yg fillable, pake guarded
    kalau lebih banyak guarded, pake fillable
    biar simple + sedikit tulisnya 
    */
    public $timestamps = false;
}
