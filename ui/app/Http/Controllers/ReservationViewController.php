<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ReservationCustomer;
use App\Model\Table;
use Auth;
use DB;

class ReservationViewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listreserve = ReservationCustomer::all();
        return view('Reservation.viewReservation', ['listreserve'=>$listreserve]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user()->id;
        $listable = Table::all();
        $listreserve = ReservationCustomer::find($id);
        return view('Reservation.approveReservation', ['listreserve'=>$listreserve], ['listtable'=>$listable]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $reserve = DB::table('customer_reservation')->where('id','=',$id)->delete();
        return redirect('viewreservation');
    }
}
