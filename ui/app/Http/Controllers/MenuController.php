<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use ui\app\domain\masterdata\service\Menu;
use App\Model\Menu;
use Auth;
use DB;
use Session;

class MenuController extends Controller
{
    private $menuService;

    /**
     * Menampilkan daftar Menu yang aktif
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listmenu = Menu::all();
        $makanan = DB::table('menu')
        ->select('*')
        ->where('menu.type','=','makanan')
        ->get();
        
        $minuman = DB::table('menu')
        ->select('*')
        ->where('menu.type','=','minuman')
        ->get();

        $desert = DB::table('menu')
        ->select('*')
        ->where('menu.type','=','dessert')
        ->get();
        $id = Session::get('order_id');
        // session()->flash('order_id', $id); // Store it as flash data.
        // $id = session()->keep(['untuk_order_id']);

        return view('Menu.menuView', ['listmenu' => $listmenu,
                                      'listmakanan' => $makanan,
                                      'listminuman'=> $minuman,
                                      'listdessert'=> $desert,
                                      'id' => $id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Menu.addMenu');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //session get id
        $user = Auth::user()->id;
        $path = "images/";
        $file = $request->file('file');
        $menu = new Menu;
        $menu->createdBy = $user;
        $menu->name = $request->name;
        $menu->price = $request->price;
        $menu->type = $request->type;
        $menu->memo =  $request->memo;
        $menu->url = $path.$file->getClientOriginalName();
        $menu->save();

        $file->move($path,$file->getClientOriginalName());

        return redirect('menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detailmenu = Menu::find($id);
        
        return view('Menu.editMenu',['detailmenu'=> $detailmenu]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //session get id
        $user = Auth::user()->id;
        
        $menu = Menu::find($id);
        $menu->createdBy = $user;
        $menu->name = $request->name;
        $menu->price = $request->price;
        $menu->type = $request->type;
        $menu->memo = $request->memo;
        $menu->save();

        return redirect('menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
