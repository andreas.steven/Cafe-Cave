<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\MenuOrder;
use App\Model\Order;
use App\Model\Menu;
use DB;

class MenuOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listMenuOrder = MenuOrder::all();
        $order = Order::all();
        $id = session()->get('order_id');
        $user_cart = DB::table('menu_order')
        ->join('orders','menu_order.order_id','=','orders.id')
        ->join('menu','menu_order.menu_id','=','menu.id')
        ->join('discount','orders.disc_id','=','discount.id')
        ->selectRaw('sum(menu_order.qty) as qty,
                        sum(menu_order.subtotal) as subtotal,
                        menu.price,
                        menu.memo,
                        menu.name,
                        menu.url,
                        menu_order.id,
                        menu_order.menu_id,
                        menu_order.order_id,
                        discount.rate')
        
        ->groupBy('menu_order.order_id','menu_order.menu_id')
        ->where('orders.id','=',$id)
        ->get();
        // dd($listMenuOrder);
        // dd($user_cart);
        return view ('MenuOrder.cart',['listmenuorder'=>$listMenuOrder,
                                            'user_cart' =>$user_cart]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listorder = Order::all();
        $listmenu = Menu::all();
        return view ('MenuOrder.addMenuOrder',['listorder'=>$listorder,
                                                'listmenu'=>$listmenu]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $menuorder = new MenuOrder;

        $temp = $request->menu;
        $menu = $users = DB::table('menu')->where('id', '=', $temp)->get();
        $price = $menu[0]->price;
        $subtotal = $price * $request->qty;
        $menuorder->order_id = $request->order;
        $menuorder->menu_id = $request->menu;
        // $menuorder->order_id = $id;
        // $menuorder->menu_id = $menu_id;
        $menuorder->qty = $request->qty;        
        $menuorder->subtotal = $subtotal;
        $menuorder->save();
        // $id = session()->get('order_id');
        // dd($id);
        return redirect('menu');
    }

    public function store_user(Request $request)
    {

        $menuorder = new MenuOrder;
        $qty = $request->qty;
        $menu_id = $request->menuid;

        $menu = DB::table('menu')->select('price')->where('id','=',$menu_id)->get();
        $price = $menu[0]->price;
        $subtotal= $qty*$price;
        
        $menuorder->order_id = session()->get('order_id');
        $menuorder->menu_id = $menu_id;
        $menuorder->qty = $request->qty;        
        $menuorder->subtotal = $subtotal;
        $menuorder->save();

        // $id = session()->get('order_id');
        // dd($id);
        return redirect('menu');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detailmenuorder = MenuOrder::find($id);
        $listmenu = Menu::all();
        return view ('MenuOrder.editMenuOrder',['detailmenuorder'=>$detailmenuorder,
                                                'listmenu'=>$listmenu]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menuorder = MenuOrder::find($id);
        $temp = $request->menu;
        $menu = $users = DB::table('menu')->where('id', '=', $temp)->get();
        $price = $menu[0]->price;
        $menuorder->menu_id = $request->menu;
        $menuorder->qty = $request->qty;
        $menuorder->subtotal = $subtotal;
        $menuorder->save();

        return redirect('menuorder');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$id2)
    {
        
        // $order = Order::find($id);
        $menu_order = DB::table('menu_order')->where('order_id', '=', $id)->where('menu_id','=',$id2)->delete();
        return redirect('cart');
    }

}
