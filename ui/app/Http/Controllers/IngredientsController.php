<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Model\Ingredient;
use App\Model\Menu;
use App\Model\Stock;

class IngredientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Ingredient::all();
        return view("Ingredient.ingredient", ['listingredients'=> $list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listmenu = Menu::all();
        $liststock = Stock::all();

        return view("Ingredient.addIngredients",['listmenu'=> $listmenu, 
                                                'liststock'=>$liststock]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ingredient = new Ingredient;
        $ingredient->menu_id = $request->menu;
        $ingredient->stock_id = $request->stock;
        $ingredient->required_stock_qty = $request->req_stock;
        $ingredient->save();

        return redirect('ingredient');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $listingredients = Ingredient::find($id);
        $listmenu = Menu::all();
        $liststock = Stock::all();
        $queryMenu = DB::table('ingredients')
                ->join('menu','ingredients.menu_id','=','menu.id')
                ->join('stock','ingredients.stock_id','=','stock.id')
                ->select('menu.name')
                ->where('ingredients.id','=',$id)
                ->get();

        // dd($queryMenu);

        return view("Ingredient.editIngredient",['detailingredient'=> $listingredients],
                                                ['listmenu'=> $listmenu, 
                                                'liststock'=>$liststock,
                                                'querymenu'=> $queryMenu]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ingredient = Ingredient::find($id);
        $ingredient->stock_id = $request->stock;
        $ingredient->required_stock_qty = $request->req_stock;
        $ingredient->save();

        return redirect('ingredient');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
