<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $queryMenu = DB::table('_history')
                ->join('orders','_history.order_id','=','order_id')
                ->join('menu_order','orders.id','=','menu_order.id')
                ->join('menu','menu_order.menu_id','=','menu.id')
                ->select('_history.id', 'menu_order.order_id', 'orders.date')
                ->get();

        return view('History.history',['listquery'=> $queryMenu]);
    }



}
