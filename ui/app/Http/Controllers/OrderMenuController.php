<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Order;
use App\Model\Discount;
use App\Model\History;
use App\Model\MenuOrder;
use DB;
use Redirect;
use Session;
use DateTime;

class OrderMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listorder = Order::all();
        $listmenuorder = MenuOrder::all();
        // $temp = 
        // $total = DB::table('menu_order')->groupBy('order_id')->sum('subtotal');
        
        // dd($listmenuorder);
        return view('Order.order',['listorder'=> $listorder]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listdiscount = Discount::all();
        return view('Order.addOrder', ['listdiscount'=> $listdiscount]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $order = new Order;
        $history = new History;
        $now = new DateTime();
        $total = 0;
        $status= "pending";
        $order->disc_id = $request->discount;
        $order->date = $now;
        $order->total = $total;
        $order->status=$status;
        $order->save();
        $history->order_id = $order->id;
        $history->save;
        // session()->flash('order_id', $id); // Store it as flash data.
        $id = $order->id;
        // dd($id);
        Session::put('order_id',$id);

        return Redirect::to('menu')->with(['id'=>$id]);
        // return redirect('menu',['id'=>$id,'halo'=>'hai']);
        // return redirect('menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detailorder = Order::find($id);
        $listdiscount = Discount::all();
        return view('order.editOrder',['detailorder'=> $detailorder], ['listdiscount'=> $listdiscount]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $total = DB::table('menu_order')->groupBy('order_id')->sum('subtotal');
        $order->disc_id = $request->discount;
        $order->total = $total;
        $order->save();

        return redirect('order');
    }

    public function payment($id,$id2){
        $order = Order::find($id);
        $total = $id2;
        $order->total = $total;
        $order->status= "paid";
        $order->save();
        
        session()->forget('order_id');

        return redirect('menu');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
