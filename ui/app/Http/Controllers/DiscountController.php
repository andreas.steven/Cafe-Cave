<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Discount;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listdiscount = Discount::all();
        //  dd($listdiscount);
        return view('Discount.discount',['listdiscount'=> $listdiscount]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Discount.addDiscount');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $discount= new discount;
        $discount->rate = $request->rate;
        $discount->init_date = $request->init_date;
        $discount->end_date = $request->end_date;
        $discount->save();

        return redirect('discount');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detaildiscount = Discount::find($id);
        
        return view('Discount.editDiscount',['detaildiscount'=> $detaildiscount]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $discount = Discount::find($id);
        $discount->rate= $request->rate;
        $discount->init_date = $request->init_date;
        $discount->end_date = $request->end_date;
        $discount->save();

        return redirect('discount');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
