<?php

namespace ui\app\domain\masterdata\entity;

use Illuminate\Database\Eloquent\Model;

/**
 * Class entity Menu
 */
class Menu extends Model
{
    //protected $table = 'menus';
    protected $primaryKey = 'id';

    protected $fillable = ['harga', 'nama'];
}
