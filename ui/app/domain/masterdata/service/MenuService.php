<?php

namespace ui\app\domain\masterdata\service;

use ui\app\domain\masterdata\repository\MenuRepository;

class MenuService
{
    /**
     * Mengembalikan seluruh data menu yg tersimpan di database.
     */
    public function getAllMenus()
    {
        return MenuRepository::findAll();
    }

    /**
     * Mengembalikan daftar menu yang aktif dijual.
     */
    public function getAllActiveMenus()
    {
        return MenuRepository::findAllActive();
    }
}
