<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/homecafe', function () {
    return view('index');
});
Route::get('/blog', function () {
    return view('blog');
});
Route::get('/services', function () {
    return view('services');
});

// Route::get('/abt', function () {
//     $judul = 'Hello World';
//     return view('about',['nama' = $nama]);
// });

Route::get('/menu', 'MenuController@index');
// Route::get('/Reserv ', 'ReservationController@index');
Route::get('/ordermenu', 'OrderMenuController@index');

// Stock
Route::get('/stock', 'StockController@index');
Route::get('createStock', 'StockController@create');
Route::get('/stock/{id}/edit', 'StockController@edit');
Route::put('/stock/{id}', 'StockController@update');
Route::post('/stock', 'StockController@store');

// Discount
Route::get('/discount', 'DiscountController@index');
Route::get('createDiscount' , 'DiscountController@create');
Route::get('/discount/{id}/edit', 'DiscountController@edit');
Route::put('/discount/{id}', 'DiscountController@update');
Route::post('/discount', 'DiscountController@store');

// Table
Route::get('/table', 'TableController@index');
Route::get('createTable', 'TableController@create');
Route::get('/table/{id}/edit', 'TableController@edit');
Route::put('/table/{id}', 'TableController@update');
Route::post('/table', 'TableController@store');

// History
Route::get('/history', 'HistoryController@index');

// Menu
Route::get('/menu', 'MenuController@index');
Route::get('/menu/{id}', 'MenuController@index');
Route::get('createMenu', 'MenuController@create');
Route::get('/menu/{id}/edit', 'MenuController@edit');
Route::put('/menu/{id}', 'MenuController@update');
Route::post('/menu', 'MenuController@store');

// Order
Route::get('/order', 'OrderMenuController@index');
Route::get('createOrder', 'OrderMenuController@create');
Route::get('/order/{id}/edit', 'OrderMenuController@edit');
Route::put('/order/{id}', 'OrderMenuController@update');
Route::post('/order', 'OrderMenuController@store');

// Payment
Route::put('/payment/{id}/{id2}/payment', 'OrderMenuController@payment');

// Ingredient
Route::get('/ingredient', 'IngredientsController@index');
Route::get('/createIngredient', 'IngredientsController@create');
Route::get('/ingredient/{id}/edit', 'IngredientsController@edit');
Route::put('/ingredient/{id}', 'IngredientsController@update');
Route::post('/ingredient', 'IngredientsController@store');

// Review
Route::get('/review', 'ReviewController@index');
Route::get('/createReview', 'ReviewController@create');
Route::post('/review', 'ReviewController@store');

// Menu Order
Route::get('/menuorder', 'MenuOrderController@index');
Route::get('/createMenuOrder', 'MenuOrderController@create');
Route::get('/menuorder/{id}/edit', 'MenuOrderController@edit');
Route::put('/menuorder/{id}', 'MenuOrderController@update');
Route::post('/menuorder', 'MenuOrderController@store');
Route::delete('/menuorder/{id}/{id2}/delete', 'MenuOrderController@destroy');

// Employee
Route::get('/employee','UserController@index');
Route::get('/employee/{id}/edit','UserController@edit');
Route::put('/employee/{id}', 'UserController@update');

Route::post('/menuordercust', 'MenuOrderController@store_user');

Route::get('/cart', 'MenuOrderController@index');




// Reservation
Route::get('/reservation', 'ReservationController@index');
Route::post('/reserve', 'ReservationController@store');

// Reservation View
Route::get('/viewreservation', 'ReservationViewController@index');
Route::get('/approve/{id}/edit', 'ReservationViewController@edit');
Route::delete('/approve/{id}/delete', 'ReservationViewController@destroy');


// Auth
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'HomeController@index')->name('home');

Route::get('/homecafe', function () {
    return view('home');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});
